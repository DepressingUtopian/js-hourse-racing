function game() {
    return {
        horses: [],
        playerScore: 100,
        rates: [],
        /**
         * Функция возвращает целочисленное число в пределах заданного интеревала
         * @param {number} min Минимальное значение интервала
         * @param {number} max Максимальное значение интервала
         */
        getRandomInt: function (min, max) {
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
        },
        /**
         * Функция ищет есть ли среди ставок лошадь которая выйграла и возвращает объект ставки
         * @param {string} nameHorse - имя лошади
         */
        searchRateWhichWin: function (nameHorse) {
            return this.rates.find((rate) => { if (rate.nameHorse === nameHorse) return rate })
        },

        /**
         * Функция создает объект лошади и возращает его
         * @param {string} name Имя лошади
         */
        createHorse: function (name) {
            return {
                name: name,
                run: () => {
                    return new Promise((resolve, reject) => {
                        const runTime = this.getRandomInt(500, 4000);
                        setTimeout(() => { resolve({ name: name, runTime: runTime }) }, runTime);
                    })
                }
            };
        },
        /**
         * Функция инициализирует массив с объектами представляющие лошадей
         * @param {string} name Имя или имена лошадей
         */
        initHorses: function () {
            for (let i = 0; i < arguments.length; i++)
                this.horses.push(this.createHorse(arguments[i]));
        },
        /**
         * Сортирует массив результатов гонок по времени финиша
         * @param {[]} array массив представляющий результаты гонок
         */
        sortHorsesByPlaceOfTime: function (array) {
            return array.sort((a, b) => {
                if (a.runTime > b.runTime) {
                    return 1;
                }
                if (a.runTime < b.runTime) {
                    return -1;
                }
                // a должно быть равным b
                return 0;
            });
        },
        resetParams() {
            this.horses = [];
            this.playerScore = 100;
            this.rates = [];
        }
    }
}
const gameHandler = game();
newGame();
/**
 * Функция выводи список лошадей
 */
function showHorses() {
    gameHandler.horses.forEach(horse => {
        console.log(horse.name);
    });

}
/**
 * Функция выводи список лошадей
 */
function showAccount() {
    console.log(`Счет игрока: ${gameHandler.playerScore}`);
}

function setWager(nameHorse, sumRates) {
    if (sumRates <= gameHandler.playerScore)
        gameHandler.rates.push({ sumRates: sumRates, nameHorse: nameHorse })
    else
        console.error(`Невозможно поставить ставку! Не хватает ${sumRates - gameHandler.playerScore}`);
}


function startRacing() {
    let promiseArray = [];
    if (!gameHandler.horses)
        console.log('Лошадей нет! Некому бежать!')
    else
        promiseArray = gameHandler.horses.map(horse => horse.run());

    if (promiseArray) {
        Promise.race(promiseArray)
            .then(result => { return result; })
            .then(winnerHorse => {
                const winRate = gameHandler.searchRateWhichWin(winnerHorse.name);
                if (winRate) {
                    console.log(`Игрок поставил на ${winnerHorse.name} и выйграл: ${winRate.sumRates * 2}`);
                    gameHandler.playerScore += winRate.sumRates * 2;
                }
                console.log(`Счет игрока: ${gameHandler.playerScore}`);
                console.log(`Результаты забега`);
                Promise.all(promiseArray).then(resultRunArray => {
                    const sortResult = gameHandler.sortHorsesByPlaceOfTime(resultRunArray);
                    for (let i = 0; i < sortResult.length; i++)
                        console.log(`Место ${i + 1} -> Лошадь по кличке ${sortResult[i].name} финишировала c ${sortResult[i].runTime}!`)
                });
                //Очистка данных ставок
                gameHandler.rates = [];
            }

            )
    };
}

function newGame() {
    gameHandler.resetParams();
    gameHandler.initHorses('Август', 'Жазель', 'Тайга', 'Аметист');
}